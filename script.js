document.addEventListener('DOMContentLoaded', () => {
  function openMobileMenu(event) {
    event.stopPropagation()
    document.querySelector('#menu-md-btn').classList.toggle('show-menu')
  }
  document.querySelector('.menu-md').addEventListener('click', openMobileMenu)

  let fetchedCats = []
  let likedCats = []

  if (localStorage.getItem('likedCats')) {
    likedCats = JSON.parse(localStorage.getItem('likedCats'))
  }

  // Fetch and render cats

  function fetchCats() {
    fetch('https://api.thecatapi.com/v1/images/search?limit=25', {
      method: 'GET',
      headers: {
        'x-api-key': '9db3f588-9423-46d9-aabf-842453801d84',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        document.querySelector('.loader').style.display = 'none'
        fetchedCats = data
        fetchedCats.map((cat) => {
          let catCard = document.createElement('div')
          catCard.className = 'cat-card'
          catCard.innerHTML = `<div><img src='${cat.url}'/><div/><div class='cat-title' id=${cat.id}>	
          &#128077 Like</div>`
          document.querySelector('.cats').append(catCard)
        })
        document
          .querySelectorAll('.cat-title')
          .forEach((button) => button.addEventListener('click', likeCatButton))
      })
  }

  fetchCats()

  // Like button function

  function likeCatButton(event) {
    fetchedCats.map((fetchedCat) => {
      if (fetchedCat.id === event.target.id) {
        likedCats = [...likedCats, fetchedCat]
      }
    })
    event.target.classList.add('unlike')
    event.target.innerHTML = '&#128078 Dislike'
    event.target.removeEventListener('click', likeCatButton)
    event.target.addEventListener('click', dislikeCatButton)
    localStorage.setItem('likedCats', JSON.stringify(likedCats))
  }

  // Dislike button function

  function dislikeCatButton(event) {
    let dislikedCatId = event.target.id
    let newLikedCats = likedCats.filter(
      (dislikeCat) => dislikeCat.id !== dislikedCatId
    )
    console.log(newLikedCats)
    likedCats = [...newLikedCats]
    event.target.classList.remove('unlike')
    event.target.innerHTML = '&#128077 Like'
    event.target.removeEventListener('click', dislikeCatButton)
    event.target.addEventListener('click', likeCatButton)
    localStorage.setItem('likedCats', JSON.stringify(likedCats))
    if (likedCats.length === 0) {
      localStorage.clear()
    }
  }
})
