document.addEventListener('DOMContentLoaded', () => {
  // Menu

  function openMobileMenu(event) {
    event.stopPropagation()
    document.querySelector('#menu-md-btn').classList.toggle('show-menu')
  }
  document.querySelector('.menu-md').addEventListener('click', openMobileMenu)

  let likedCats = []

  if (localStorage.getItem('likedCats')) {
    likedCats = JSON.parse(localStorage.getItem('likedCats'))
    renderLikedCats()
  } else {
    document.querySelector(
      '.cats'
    ).innerHTML = `<div class='loader'><h1>You have no liked cats :( </h1></div>`
  }

  // Render Liked cats

  function renderLikedCats() {
    if (localStorage.getItem('likedCats')) {
      document.querySelector('.cats').innerHTML = null
      likedCats = JSON.parse(localStorage.getItem('likedCats'))
      likedCats.map((cat) => {
        let catCard = document.createElement('div')
        catCard.className = 'cat-card'
        catCard.innerHTML = `<div><img src='${cat.url}'/><div/><div class='cat-title unlike' id=${cat.id}>	
        &#128078 Dislike</div>`
        document.querySelector('.cats').append(catCard)
      })
      document
        .querySelectorAll('.cat-title')
        .forEach((button) => button.addEventListener('click', dislikeCatButton))
    } else {
      document.querySelector(
        '.cats'
      ).innerHTML = `<div class='loader'><h1>You have no liked cats :( </h1></div>`
    }
  }

  // Dislike button function

  function dislikeCatButton(event) {
    let dislikedCatId = event.target.id
    let newLikedCats = likedCats.filter(
      (dislikeCat) => dislikeCat.id !== dislikedCatId
    )
    likedCats = [...newLikedCats]
    console.log(likedCats)
    event.target.removeEventListener('click', dislikeCatButton)
    localStorage.setItem('likedCats', JSON.stringify(likedCats))
    if (likedCats.length === 0) {
      localStorage.clear()
    }
    renderLikedCats()
  }
})
